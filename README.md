# What is it
I want to log some stats of my local system so I can see how often I use full
capacity of my CPU or hit thermal throttle. I know there are lots of tools for
this, like [munin](https://munin-monitoring.org/) but I want to keep it simple
plus I like creating things.

# How to run
```
cd local-load-logging/
sudo node index.mjs
# ctrl-c to kill the process
# look at data.csv
```

# TODO
- figure out how to chart the results
  - could load to influx on-demand when I want to visualise
  - if influx is lightweight enough, could log directly to it

# Notes
Grafana and influx use ~90MB of RAM each after a fresh start, at idle, with no
data.

Sending data to influx looks easy enough, it takes CSV:
https://docs.influxdata.com/influxdb/v2.2/write-data/developer-tools/api/#send-a-write-request

```
curl --request POST \
"http://localhost:8086/api/v2/write?org=YOUR_ORG&bucket=YOUR_BUCKET&precision=ns" \
  --header "Authorization: Token YOUR_API_TOKEN" \
  --header "Content-Type: text/plain; charset=utf-8" \
  --header "Accept: application/json" \
  --data-binary '
    airSensors,sensor_id=TLM0201 temperature=73.97038159354763,humidity=35.23103248356096,co=0.48445310567793615 1630424257000000000
    airSensors,sensor_id=TLM0202 temperature=75.30007505999716,humidity=35.651929918691714,co=0.5141876544505826 1630424257000000000
    '
```
...and there's a NodeJS lib.
