import fs from 'fs'
import util from 'node:util'
import { exec as cpExec } from 'node:child_process'
const exec = util.promisify(cpExec)

// Config:
const intervalSeconds = 3
const logFile = './data.csv'

function getLoadData() {
  const loadDataRaw = fs.readFileSync('/proc/loadavg')
  const matches = /(\d\.\d\d) (\d\.\d\d) (\d\.\d\d)/.exec(loadDataRaw)
  return {
    oneMinLoad: matches[1],
    fiveMinLoad: matches[2],
    fifteenMinLoad: matches[3],
  }
}

async function getFreqData() {
  // apparently hyperthread cores can scale separately, or at least appear to,
  // from their sibling on the physical core.
  const { stdout } = await exec(`
    cat /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_cur_freq
  `)
  return stdout.trim().split('\n')
}

async function getPowerData() {
  // note: you probably need root to run this command
  const { stdout } = await exec(`
    turbostat -s PkgWatt,CorWatt,RAMWatt --Summary --quiet -n 1 -i 0.001 \
      | tail -n1
  `)
  const [pkgWatt, coreWatt, ramWatt] = stdout.trim().split('\t')
  return {pkgWatt, coreWatt, ramWatt}
}

async function getTempData() {
  const { stdout } = await exec(`
    sensors -j thinkpad-isa-0000 coretemp-isa-0000
  `)
  const endIndex = (/^}$/m.exec(stdout)).index + 1
  const ti0 = (() => {
    const raw = stdout.substr(0, endIndex)
    const parsed = JSON.parse(raw)
    return parsed['thinkpad-isa-0000']
  })()
  const ci0 = (() => {
    const parsed = JSON.parse(stdout.substr(endIndex))
    return parsed['coretemp-isa-0000']
  })()
  const result = {
    cpuTemp: ti0.CPU.temp1_input,
    gpuTemp: ti0.GPU.temp2_input,
    fan1: ti0.fan1.fan1_input,
    fan2: ti0.fan2.fan2_input,
    packageTemp: ci0['Package id 0'].temp1_input,
  }
  for (let i = 0; i <= 5; i += 1) {
    result[`core${i}Temp`] = ci0[`Core ${i}`][`temp${i+2}_input`]
  }
  return result
}

function writeRow(row, theFile = logFile) {
  const csvRowStr = row.join(',')
  if (theFile) {
    fs.appendFileSync(theFile, csvRowStr + '\n')
  }
  console.log(csvRowStr)
}

async function doCapture() {
  const [loadData, freqData, powerData, tempData] = await Promise.all([
    getLoadData(), getFreqData(), getPowerData(), getTempData(),
  ])
  writeRow([
    Date.now(),
    loadData.oneMinLoad,
    loadData.fiveMinLoad,
    loadData.fifteenMinLoad,
    ...freqData,
    powerData.pkgWatt,
    powerData.coreWatt,
    powerData.ramWatt,
    tempData.cpuTemp,
    tempData.gpuTemp,
    tempData.fan1,
    tempData.fan2,
    tempData.packageTemp,
    tempData.core0Temp,
    tempData.core1Temp,
    tempData.core2Temp,
    tempData.core3Temp,
    tempData.core4Temp,
    tempData.core5Temp,
  ])
}

async function writeHeader(isScreenOnly) {
  const freqHeaders = []
  const freqData = await getFreqData()
  const theFile = isScreenOnly ? null : logFile
  writeRow([
    'unixEpoch',
    'oneMinLoad',
    'fiveMinLoad',
    'fifteenMinLoad',
    ...freqData.map((_,i) => `freq${i}`),
    'pkgWatt',
    'coreWatt',
    'ramWatt',
    'cpuTemp',
    'gpuTemp',
    'fan1',
    'fan2',
    'packageTemp',
    'core0Temp',
    'core1Temp',
    'core2Temp',
    'core3Temp',
    'core4Temp',
    'core5Temp',
  ], theFile)
}

console.log(`Writing to ${logFile} every ${intervalSeconds} seconds`)
try {
  const {size} = fs.statSync(logFile)
  let isScreenOnly = size > 0
  await writeHeader(isScreenOnly)
} catch (e) {
  if (e.code === 'ENOENT') {
    await writeHeader(false)
  } else {
    throw e
  }
}
setInterval(doCapture, intervalSeconds * 1000)
